package nope.space.wikigame

import java.net.URL
import java.util.*

class Game private constructor(val sourceLocation: URL, val destinationLocation: URL, val id: UUID) {
    fun save(): Game {
        storage[id] = this
        return this
    }

    companion object {
        var storage = mutableMapOf<UUID, Game>()

        operator fun invoke(sourcePage: URL, destinationPage: URL) : Game {
            return Game(sourcePage, destinationPage, UUID.randomUUID())
        }
        fun get(id: UUID) : Game? {
            return storage[id]
        }

        fun all(): List<Game> = storage.values.toList()
    }
}