package nope.space.wikigame

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WikigameApplication

fun main(args: Array<String>) {
	runApplication<WikigameApplication>(*args)
}
