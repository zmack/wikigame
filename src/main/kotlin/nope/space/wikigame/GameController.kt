package nope.space.wikigame

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.net.URL
import java.util.*

data class GamePayload(val sourceLocation: String, val destinationLocation: String)

@RestController
@RequestMapping("/games")
class GameController(
    @Autowired val travelService: WikiTravel
) {
    @GetMapping("/{id}")
    fun get(@PathVariable("id") id : UUID) : Mono<Game> {
        Game.get(id)?.also { game ->
            return Mono.just(game)
        }
        return Mono.never()
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id : UUID) : Mono<List<String>> {
        Game.get(id)?.also { game ->
            val urls = travelService.findPath(game)
            return Mono.just(urls)
        }

        return Mono.never()
    }

    @PostMapping("/")
    fun create(@RequestBody body: GamePayload) : Mono<Game> {
        val game = Game(URL(body.sourceLocation), URL(body.destinationLocation)).save()
        return Mono.just(game)
    }

    @GetMapping("/")
    fun list() : Flux<Game> {
        return Flux.fromIterable(Game.all())
    }
}