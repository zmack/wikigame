package nope.space.wikigame

import org.jsoup.Jsoup
import org.springframework.stereotype.Service
import java.net.URL

@Service
class WikiTravel {
    val anchorFilter = Regex("#.*$")
    fun findPath(game: Game): List<String> {
        return visit(game.sourceLocation)
    }

    fun visit(url: URL) : List<String> {
        val document = Jsoup.connect(url.toString()).get()

        val anchors = if (url.host.contains("wikipedia.org")) {
            document.getElementsByTag("a")
        } else {
            document.select("#content a")
        }

        return anchors
            .map { URL(url, it.attr("href").replace(anchorFilter, "")) }
            .filter {
                it.host == "en.wikipedia.org" && // everything that's an english wikipedia page
                !it.path.startsWith("/wiki/File:") && // but not a file
                !it.path.startsWith("/w/index.php") && // or an incomplete page
                !it.path.startsWith("/wiki/Template:") && // or a template page
                !it.path.startsWith("/wiki/Category:") && // or a category page, those are dumb too
                !it.path.startsWith("/wiki/Special:") && // or special, not even sure what that is
                !it.path.startsWith("/wiki/Talk:") && // or a talk page, those are the worst
                !it.path.startsWith("/wiki/Template_talk:") && // or a template talk page, how are these a thing
                !it.path.startsWith("/wiki/Wikipedia:") && // or a wikipedia core page
                !it.path.startsWith("/wiki/Help:") && // or a wikipedia help page
                !it.path.startsWith("/wiki/Portal:") // or a wikipedia portal page
            }.map { it.toExternalForm() }.distinct()
    }
}